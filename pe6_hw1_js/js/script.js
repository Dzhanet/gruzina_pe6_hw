// Обьяснить своими словами разницу между обьявлением переменных через var, let и const.
// Все эти слова используются для объявления переменных в js. 
// var похожа по принципу действия на let, но имеет ряд недостатков, const задается в случае, если мы далее не планируем менять эту переменную.
// Почему объявлять переменную через var считается плохим тоном?
// var является устаревшей, в современном стандарте ES2015 используется let и const. Основные недостатки var:
// 1) ограничивается либо же функцией, либо же всем скриптом, ее невозможно применить к конкретному блоку.
// 2) независимо от месторасположения var в функции, она обрабатывается в начале исполнения функции, так же var допускает повторное объявление.

"use strict";

let userName;
let userAge;

do {userName = prompt ("Please enter your name");

} while (userName === "" || !isNaN(userName));

do {userAge = prompt ("Please enter your age");

} while (!userAge || isNaN(+userAge));

// while (userName === "")
// {
//     userName = prompt ("Please enter your name");
// }
// while (!userAge || isNaN(+userAge) ||typeof +userAge !== "number")
// {
//       userAge = prompt ("Please enter your age");
// }

if (userAge < 18) {
    alert("You are not allowed to visit this website")
}
else if (userAge >=18 && userAge <=22) {
    let userAgeConfirm = confirm("Are you sure you want to continue?")
    if (userAgeConfirm){
        alert("Welcome, " +userName);
    }
    else{
        alert("You are not allowed to visit this website")   
    }
}
else {
    alert("Welcome, " +userName)
}