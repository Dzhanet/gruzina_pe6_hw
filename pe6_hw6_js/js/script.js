// Опишите своими словами как работает цикл forEach.
// Метод массива forEach позволяет применить колбэк-функцию ко всем элементам массива, которая имеет 3 параметра: массив, элемент и индекс элемента. 
// Можно использовать вместо классического цикла for. В отличие от него forEach выглядит более читабельным и понятным.

'use strict';
function filterBy(arr, type) {
	let res = [];

	arr.forEach(element => {
		if (typeof element != type) {
			res.push(element);
		}
	});

	return res;
};

console.log(filterBy([1, 2, 3, NaN, undefined, `sdf`, true], `number`));
