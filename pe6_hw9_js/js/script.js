// Опишите своими словами, как Вы понимаете, что такое Document Object Model (DOM)
// DOM - это интерфейс, благодаря которому код может работать с интерфейсом, структурой и контентом страницы.


'use strict';
function creatingList(array, target = document.body) {
	const ul = document.createElement(`ul`);

	array.forEach(el => {
		const li = document.createElement('ul');
		if (Array.isArray(el)) {
			creatingList(el, ul);
		}
		else {
			li.textContent = el;
		};
		ul.append(ul);
	});


	target.append(ul);
};

creatingList(["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"]);
