// Опишите своими словами, как Вы понимаете, что такое Document Object Model (DOM)
// DOM - это документная модель объекта, благодаря которой код может работать с интерфейсом, структурой и контентом страницы.


'use strict';
const default = document.body;

function arrWriter(arr, where=default){
    const toMap = arr.map((el)=>`<li>${el}</li>`);
    const toJoin = toMap.join("");
    return where.innerHTML = `${toJoin}`;
};

const ul = document.createElement("ul");
document.body.prepend(ul);
const array = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];


arrWriter(array, ul);


