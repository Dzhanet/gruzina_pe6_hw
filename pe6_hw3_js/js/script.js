// Описать своими словами для чего вообще нужны функции в программировании.
// Функции - это блок кода, который можно использовать в программе во всех местах, где это нужно и неограниченное количество раз. 
// При этом, если  нужно внести изменения - то их достаточно сделать в самой функции один раз, и везде применится. ТО есть функции облегчают трудозатраты при разработке.
// Описать своими словами, зачем в функцию передавать аргумент.
// Значения, которые передаются при вызове функции, называются аргументами. 
// Собственно говоря, это те данные, с которыми функция будет что-то делать исходя из ее предназначения.

"use strict";
let num1;
let num2; 
let operator;

num1 = prompt(`Enter first number`);
num2 = prompt(`Enter second number`);
operator = prompt(`Enter operator`);

while (
	!num1 || !num1.trim() || isNaN(+num1)
	|| !num2 || !num2.trim() || isNaN(+num2)) {
	num1 = prompt(`Enter first number`);
	num2 = prompt(`Enter second number`);
}
function calc(num1, num2, operator) {

	num1 = +num1;
	num2 = +num2;

	switch (operator) {
		case `+`:
			return `${num1} + ${num2} = ${num1 + num2}`;
			break;
		case `-`:
			return `${num1} - ${num2} = ${num1 - num2}`;
			break;
		case `*`:
			return `${num1} * ${num2} = ${num1 * num2}`;
			break;
		case `/`:
			return `${num1} / ${num2} = ${num1 / num2}`;
			break;

		default:
			return `Enter operator`
			break;
	}
};

console.log(calc(num1, num2, operator));

